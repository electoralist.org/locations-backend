package org.electoralist.locations.search.indexing

import com.fasterxml.jackson.core.`type`.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper

import scala.io.Source

class OutlineReader {
  def makeOutline(fileName: String): java.util.HashMap[String, Object] = {
    val jsonSource = Source.fromFile(fileName)
    val mapper = new ObjectMapper()
    val typeRef = new TypeReference[java.util.HashMap[String, Object]](){}
    val jsonObj = mapper.readValue(jsonSource.reader(), typeRef).asInstanceOf[java.util.HashMap[String, java.util.ArrayList[java.util.HashMap[String, java.util.Map[String, Object]]]]]

    val coordinates = jsonObj.get("features").get(0).get("geometry").get("coordinates")

    val outlineMap = new java.util.HashMap[String, Object]();
    outlineMap.put("coordinates", coordinates)
    outlineMap.put("type", jsonObj.get("features").get(0).get("geometry").get("type").asInstanceOf[String].toLowerCase())
    outlineMap
  }
}
