package org.electoralist.locations.search.indexing.clients

import org.electoralist.JsonUtil
import org.electoralist.locations.search.indexing.rest.models.DistrictRace
import sttp.client3.{HttpURLConnectionBackend, asByteArray, basicRequest}
import sttp.model.Uri

import java.net.URI

case class LocationsRestClientConfig(baseUrl: String)

class LocationsRestClient(locationsRestClientConfig: LocationsRestClientConfig) {
  def getDistrictsByStateCode(stateCode: String): List[DistrictRace] = {
    val url = s"${locationsRestClientConfig.baseUrl}/districts/races/byState/${stateCode}"
    val jUri = new URI(url)
    val sUri = Uri(jUri)
    val request = basicRequest.get(sUri).response(asByteArray)
    val backend = HttpURLConnectionBackend()
    val response = request.send(backend)

    val districts = response.body match {
      case Left(bad) => throw new RuntimeException(s"Sad trombone ${bad}")
      case Right(good) => JsonUtil.fromJson[List[DistrictRace]](good)
    }
    println(s"These are your districts: ${districts}")
    districts
  }
}
