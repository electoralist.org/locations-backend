package org.electoralist.locations.services

import org.electoralist.locations.models.SearchParameters
import org.electoralist.locations.search.indexing.search.models.SearchResults
import org.electoralist.locations.validation.ValidationError

class SearchService(namedDistrictSearchService: NamedDistrictSearchService,
                    stateSearchService: StateSearchService,
                    locationSearchSevice: LocationSearchSevice,
                    keywordSearchService: KeywordSearchService) {
  def search(searchParams: SearchParameters): SearchResults = {
    searchParams match {
      case SearchParameters(Some(lat), Some(lon), _) =>
        latLongSearch(java.lang.Double.parseDouble(lat), java.lang.Double.parseDouble(lon))
      case SearchParameters(_, _, Some(query)) =>
        ???
      case _ =>
        throw new ValidationError
    }
  }

  private def latLongSearch(lat: Double, lon: Double): SearchResults = {
    locationSearchSevice.search(lat, lon)
  }

  private val stateDistrictCodePattern = "([a-zA-Z]{2})[-]([0-9]+)".r
  private val stateAtLargeCodePattern = "([a-zA-Z][a-zA-Z])[-]AL".r
  private val stateAbbreviationPattern = "([a-zA-Z]{2}})".r

  private def querySearch(query: String): SearchResults = {
    query match {
      case stateDistrictCodePattern(stateAbbreviation: String, districtNumber: String) =>
        namedDistrictSearchService.search(stateAbbreviation, districtNumber.toInt)
      case stateAtLargeCodePattern(stateAbbreviation) =>
        namedDistrictSearchService.search(stateAbbreviation, 1)
      case stateAbbreviationPattern(stateAbbreviation) =>
        stateSearchService.search(stateAbbreviation)
      case _ =>
        keywordSearchService.search(query)
    }
  }
}
