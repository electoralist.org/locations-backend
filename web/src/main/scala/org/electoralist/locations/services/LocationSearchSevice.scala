package org.electoralist.locations.services

import org.electoralist.locations.JsonUtil
import org.electoralist.locations.search.indexing.search.models.{DistrictRace, SearchResults, SearchResultsType}
import org.opensearch.client.{RequestOptions, RestClientBuilder, RestHighLevelClient}
import org.opensearch.action.search.SearchRequest
import org.opensearch.common.geo.ShapeRelation
import org.opensearch.geometry.Point
import org.opensearch.index.query.{GeoShapeQueryBuilder, QueryBuilder}
import org.opensearch.search.builder.SearchSourceBuilder
import org.opensearch.search.fetch.subphase.FetchSourceContext

class LocationSearchSevice(restClientBuilder: RestClientBuilder) {
  def search(lat: Double, long: Double): SearchResults = {
    val client = new RestHighLevelClient(restClientBuilder)
    val searchRequest: SearchRequest = new SearchRequest("custom-index-2")
    searchRequest.source(new SearchSourceBuilder().query(queryBuilder(lat, long)).fetchSource(new FetchSourceContext(true, Array(), Array("outline"))))
    val searchResults = client.search(searchRequest, RequestOptions.DEFAULT)
    val hits = searchResults.getHits.getHits
    val districtHits = hits.toList.map(
      hit => {
        val hitBytes = hit.getSourceRef.toBytesRef.bytes
        JsonUtil.fromJson[DistrictRace](hitBytes)
      }
    )
    SearchResults(districtHits, SearchResultsType.MIXED_RESULTS)
  }

  private def queryBuilder(lat: Double, long: Double): QueryBuilder = {
    val queryBuilder = new GeoShapeQueryBuilder("outline", new Point(long, lat)).relation(ShapeRelation.CONTAINS)
    queryBuilder
  }


}
