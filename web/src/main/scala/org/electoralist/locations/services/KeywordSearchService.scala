package org.electoralist.locations.services

import org.electoralist.locations.search.indexing.search.models.SearchResults
import org.electoralist.locations.search.indexing.search.models.SearchResultsType.MIXED_RESULTS

class KeywordSearchService {
  def search(query: String): SearchResults =
    SearchResults(Seq(), MIXED_RESULTS)
}
