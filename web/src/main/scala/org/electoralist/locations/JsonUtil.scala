package org.electoralist.locations

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.{DefaultScalaModule, ScalaObjectMapper}

object JsonUtil {
  val mapper = new ObjectMapper() with ScalaObjectMapper
  mapper.registerModule(DefaultScalaModule)
  mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

  def toJson(value: Map[Symbol, Any]): Array[Byte] = {
    toJson(value map { case (k,v) => k.name -> v})
  }

  def toJson(value: Any): Array[Byte] = {
    mapper.writeValueAsBytes(value)
  }

  def toMap[V](json:Array[Byte])(implicit m: Manifest[V]) = fromJson[Map[String,V]](json)

  def fromJson[T](json: Array[Byte])(implicit m : Manifest[T]): T = {
    mapper.readValue[T](json)
  }
}
