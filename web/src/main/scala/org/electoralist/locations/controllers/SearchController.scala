package org.electoralist.locations.controllers

import org.electoralist.locations.models.SearchParameters
import org.electoralist.locations.services.SearchService
import org.json4s.{DefaultFormats, Formats}
import org.scalatra.{BadRequest, ScalatraServlet}
import org.scalatra.json.JacksonJsonSupport

import scala.xml.dtd.ValidationException

class SearchController(searchService: SearchService) extends ScalatraServlet with JacksonJsonSupport {
  protected implicit lazy val jsonFormats: Formats = DefaultFormats

  before() {
    contentType = formats("json")
  }

  get("/") {
    val latitude = params("lat")
    val longitude = params("lon")
    //val searchRequest = parse(request.body).extract[SearchParameters]
    try {
      searchService.search(SearchParameters(Option(latitude), Option(longitude), None))
    }
    catch
    {
      case ValidationException(ex) =>
      BadRequest()
    }
  }

}
