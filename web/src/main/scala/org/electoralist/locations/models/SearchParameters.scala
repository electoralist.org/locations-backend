package org.electoralist.locations.models

case class SearchParameters(
                             geoLat: Option[String],
                             geoLong: Option[String],
                             query: Option[String]
                           )
