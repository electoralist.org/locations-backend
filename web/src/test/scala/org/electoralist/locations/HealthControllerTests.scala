package org.electoralist.locations

import org.scalatra.test.scalatest._

class HealthControllerTests extends ScalatraFunSuite {

  addServlet(classOf[HealthController], "/*")

  test("GET / on HealthController should return status 200") {
    get("/") {
      status should equal (200)
    }
  }

}
