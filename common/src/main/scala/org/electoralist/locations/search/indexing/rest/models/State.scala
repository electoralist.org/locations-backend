package org.electoralist.locations.search.indexing.rest.models

case class State(
                  id: Int,
                  code: String,
                  name: String,
                  comments: Option[String],
                  extendedComments: Option[String]
                )
