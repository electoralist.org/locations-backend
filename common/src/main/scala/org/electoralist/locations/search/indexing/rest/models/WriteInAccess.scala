package org.electoralist.locations.search.indexing.rest.models

case class WriteInAccess(
                          id: Int,
                          name: String,
                          filingDeadline: String,
                          filingFee: Int,
                          signatureRequirement: Int,
                          comments: Option[String],
                          extendedComments: Option[String],
                          source: Option[String]
                        )
