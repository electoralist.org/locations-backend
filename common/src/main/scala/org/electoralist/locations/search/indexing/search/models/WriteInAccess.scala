package org.electoralist.locations.search.indexing.search.models

import org.electoralist.locations.search.indexing.rest.models.{WriteInAccess => RestWriteInAccess}


case class WriteInAccess(filingDeadline: String,
                        filingFee: Int,
                        signatureRequirement: Int,
                        comments: Option[String],
                        extendedComments: Option[String])

object WriteInAccess {
  def apply(restWriteInAccess: RestWriteInAccess): WriteInAccess = {
    WriteInAccess(
      restWriteInAccess.filingDeadline,
      restWriteInAccess.filingFee,
      restWriteInAccess.signatureRequirement,
      restWriteInAccess.comments,
      restWriteInAccess.extendedComments
    )
  }
}