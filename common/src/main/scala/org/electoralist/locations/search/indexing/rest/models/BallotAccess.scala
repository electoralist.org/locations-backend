package org.electoralist.locations.search.indexing.rest.models


case class BallotAccess(
                         id: Int,
                         name: String,
                         recognizedParties: Boolean,
                         filingDeadline: String,
                         filingFee: Int,
                         signatureRequirement: Int,
                         comments: Option[String],
                         extendedComments: Option[String],
                         source: Option[String]
                       )
