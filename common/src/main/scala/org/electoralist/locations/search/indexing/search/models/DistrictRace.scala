package org.electoralist.locations.search.indexing.search.models

import org.electoralist.locations.search.indexing.search.models.SearchResultType.SearchResultType

case class DistrictRace(
                       id: Int,
                       outline: Option[AnyRef],
                       resultType: String,
                       districtCodes: List[String],
                       stateCode: String,
                       electionCycleYear: Int,
                       electionDate: String,
                       comments: Option[String],
                       extendedComments: Option[String],
                       resultName: String,
                       ballotAccesses: List[BallotAccess],
                       writeInAccess: WriteInAccess
                       ) extends SearchResult
